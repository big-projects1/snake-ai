using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class showDeaths : MonoBehaviour
{
    public GameObject snek;
    private UnityEngine.UI.Text deaths_display;
    private int _deaths=0;
    private float upChance;
    private float downChance;
    private float leftChance;
    private float rightChance;
    // Start is called before the first frame update
    void Start()
    {
        deaths_display = GetComponent<UnityEngine.UI.Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _deaths = snek.GetComponent<Snek>().deaths;
        upChance = snek.GetComponent<Snek>().upThreshold;
        downChance = snek.GetComponent<Snek>().downThreshold;
        leftChance = snek.GetComponent<Snek>().leftThreshold;
        rightChance = snek.GetComponent<Snek>().rightThreshold;
        deaths_display.text = "DEATHS : " + _deaths.ToString() + "\n" + "Up/Down sensor : " + upChance.ToString("F2") + "\n" + "Right/Left sensor : " + rightChance.ToString("F2");
            //+"downSensor: " + downChance.ToString("F2") + "\n" + "leftSensor: " + leftChance.ToString("F2");
    }
}
