using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class foodScript : MonoBehaviour
{
    private Snek snakebody;
    private int pickerX;
    private int pickerY;
    private int boundX;
    private int boundY;


    private void Awake()
    {
        world_manager.onInit += CatchDimension;
    }

    private void CatchDimension(int height, int width)
    {
        boundY = width;
        boundX = height;
    }

    // Start is called before the first frame update
    void Start()
    {
        snakebody = GameObject.Find("Snek").GetComponent<Snek>();
        
        var pickerX = Random.Range(-boundX+5, boundX-3);
        var pickerY = Random.Range(-boundY+2, boundY-2);
        Vector3 pickerXY = new Vector3(pickerX, pickerY, 0);

   
       
        var counter = 0;
        
        do
        {
            
            for (int i = 0; i < snakebody.snake_body.Count; i++)
            {
                if (pickerXY == snakebody.snake_body[i].position)
                {
                    pickerX = Random.Range(-boundX + 1, boundX);
                    pickerY = Random.Range(-boundY + 1, boundY);
                    
                    pickerXY = new Vector3(pickerX, pickerY, 0);
                    return;
                }
 
            }
            counter+=1;

        } while (counter<10);
        this.gameObject.transform.position = new Vector3(pickerX, pickerY, 0);

    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(this.gameObject.transform.position.x)>=boundX || Mathf.Abs(this.gameObject.transform.position.y) >= boundY)
        {
            Destroy(this.gameObject);
            
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(this.gameObject);
     
    }

}
