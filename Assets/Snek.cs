using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Snek : MonoBehaviour
{
    //TESTING



    //SETTINGS
    public float doNothing;
    [Range(0.0f, 4.0f)]
    public float foodBias;
    [Range(0.0f, 50f)]
    public float spaceBias;
    public bool GhostMode;
    public bool drawEyes = false;

    public Transform eyes;
    public Transform bodyPrefab;
    public Transform forwardCircle;
    public Transform leftCircle;
    public Transform rightCircle;

    [HideInInspector]
    public List<Transform> snake_body;
    [HideInInspector]
    public int deaths;
    [HideInInspector]
    public float upThreshold = 25f;
    [HideInInspector]
    public float downThreshold = 50f;
    [HideInInspector]
    public float rightThreshold = 25f;
    [HideInInspector]
    public float leftThreshold = 50f;

    //MYVARS
    private Transform trans;
    private Transform foundFood;
    private bool autoPilot;
    private bool _movingHorizontal;
    private bool _movingVertical;
    private float boundX;
    private float boundY;
    private Vector3 directionForward;
    private Vector3 directionLeft;
    private Vector3 directionRight;
    private Vector3 forward;
    private Vector3 left;
    private Vector3 right;

    
    private Slider foodSlider;
    private Slider spaceSlider;
    private Slider randomSlider;
    private Button ghostButton;
    private Button senseButton;
    private Button resetButton;
    


    void Awake()
    {
        world_manager.onInit += CatchDimension;

        InitSnake();
    }

    private void Start()
    {
        deaths = 0;
        foodSlider = GameObject.Find("Food Sense").GetComponent<Slider>();
        spaceSlider = GameObject.Find("Space Sense").GetComponent<Slider>();
        randomSlider = GameObject.Find("Randomness").GetComponent<Slider>();
        ghostButton = GameObject.Find("Ghost Mode").GetComponent<Button>();
        ghostButton.onClick.AddListener(GhostClick);
        senseButton = GameObject.Find("Show Senses").GetComponent<Button>();
        senseButton.onClick.AddListener(SenseClick);
        drawEyes = false;

        resetButton = GameObject.Find("Reset").GetComponent<Button>();
        resetButton.onClick.AddListener(ResetClick);
    }

    private void CatchDimension(int width, int height)
    {
        this.boundX = width - 0.01f;
        this.boundY = height - 0.01f;
    }

    private void FixedUpdate()
    {
        _movingVertical = movingVertical();
        _movingHorizontal = movingHorizontal();
        foundFood = GameObject.FindGameObjectWithTag("food").GetComponent<Transform>();

        if (autoPilot)
        {

            getSmartDirection(trans);
        }


        for (int i = this.snake_body.Count - 1; i > 0; i--)
        {
            this.snake_body[i].position = this.snake_body[i - 1].position;
        }
        trans.position = trans.position + this.directionForward;
        Mathf.Round(trans.position.x);
        Mathf.Round(trans.position.y);
        CheckBody(trans);//Check collision with body
        handleCollision();// Check if game over
    }

    private void Update()
    {
        autoPilot = AutoPilot();
        if (!autoPilot)
        {
            getDirection();
        }

        
    }

    private void GhostClick()
    {
        if(!GhostMode)
        {
            GhostMode = true;
        }
        if(GhostMode)
        {
            GhostMode = false;
        }
    }
    private void SenseClick()
    {
        
        if(drawEyes)
        {
            drawEyes = false;
        }
        else
        {
            drawEyes = true;
        }

    }
    private void ResetClick()
    {
        SnakeReset();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "food")
        {
            grow();
        }

    }

    private void InitSnake()
    {

        trans = GetComponent<Transform>();
        this.directionForward = new Vector3(0, 1, 0);
        this.directionLeft = new Vector3(-1, 0, 0);
        this.directionRight = new Vector3(1, 0, 0);
        this.trans.position = new Vector3(0, 0, 0);
        this.snake_body = new List<Transform>();
        this.snake_body.Add(this.trans);
        autoPilot = true;
        _movingVertical = movingVertical();
        _movingHorizontal = movingHorizontal();

    }

    private void SnakeReset()
    {
        deaths++;
        var bodyparts = GameObject.FindGameObjectsWithTag("Body");
        for (int i = 0; i < bodyparts.Length; i++)
        {
            Destroy(bodyparts[i]);
        }
        this.directionForward = new Vector3(0, 1, 0);
        this.directionLeft = new Vector3(-1, 0, 0);
        this.directionRight = new Vector3(1, 0, 0);
        this.trans.position = new Vector3(0, 0, 0);
        this.snake_body = new List<Transform>();
        this.snake_body.Add(this.trans);
        autoPilot = true;
        _movingVertical = movingVertical();
        _movingHorizontal = movingHorizontal();
    }


    private bool FoodLock(Transform trans)
    {

        var foodLockHorizontal = trans.position.x;
        var foodLockVertical = trans.position.y;
        var bodyHit = false;

        if (_movingVertical)
        {
            while (Mathf.Abs(foodLockVertical) < this.boundY)
            {


                Vector3 scanLocation = new Vector3(trans.position.x, foodLockVertical + this.directionForward.y, 0);
                foreach (var bodypart in this.snake_body)
                {
                    if (bodypart.position == scanLocation)
                    {
                        bodyHit = true;
                        //print("HitVertical");
                        break;
                    }
                }
                if (bodyHit) break;
                if (scanLocation == foundFood.position)
                {
                    return true;
                }
                if (drawEyes) Instantiate(eyes, scanLocation, trans.rotation);
                foodLockVertical += directionForward.y;

            }
        }


        else if (_movingHorizontal)
        {
            while (Mathf.Abs(foodLockHorizontal) < this.boundX)
            {


                Vector3 scanLocation = new Vector3(foodLockHorizontal + this.directionForward.x, trans.position.y, 0);
                foreach (var bodypart in this.snake_body)
                {
                    if (bodypart.position == scanLocation)
                    {
                        bodyHit = true;
                        //print("HitHorizontal");
                        break;
                    }
                }
                if (bodyHit) break;
                if (scanLocation == foundFood.position)
                {
                    return true;
                }
                if (drawEyes) Instantiate(eyes, scanLocation, trans.rotation);
                foodLockHorizontal += directionForward.x;
            }
        }
        return false;
    }


    private void getSmartDirection(Transform trans)
    {
        this.doNothing = randomSlider.value;
        spaceBias = spaceSlider.value;
        foodBias = foodSlider.value;
        this.upThreshold = 25f - (spaceBias * (trans.position.y / this.boundY) - (foodBias * (foundFood.position.y - trans.position.y)));
        this.downThreshold = 50f;
        this.rightThreshold = 25f - (spaceBias * (trans.position.x / this.boundX) - (foodBias * (foundFood.position.x - trans.position.y)));
        this.leftThreshold = 50f;
        HandleWalls(trans);
        if (FoodLock(trans)) return;
        var picker = Random.Range(0.1f, maxInclusive: this.doNothing);

        if (_movingHorizontal)
        {
            if (picker < upThreshold)
            {
                this.directionForward = new Vector3(0, 1, 0);
                this.directionLeft = new Vector3(-1, 0, 0);
                this.directionRight = new Vector3(1, 0, 0);
            }
            else if (picker < downThreshold)
            {
                this.directionForward = new Vector3(0, -1, 0);
                this.directionLeft = new Vector3(1, 0, 0);
                this.directionRight = new Vector3(-1, 0, 0);
            }
        }

        if (_movingVertical)
        {
            if (picker < rightThreshold)
            {
                this.directionForward = new Vector3(1, 0, 0);
                this.directionLeft = new Vector3(0, 1, 0);
                this.directionRight = new Vector3(0, -1, 0);
            }
            else if (picker < leftThreshold)
            {
                this.directionForward = new Vector3(-1, 0, 0);
                this.directionLeft = new Vector3(0, -1, 0);
                this.directionRight = new Vector3(0, 1, 0);
            }
        }
    }

    private void CheckBody(Transform trans)
        {
           
            for (int i = this.snake_body.Count - 1; i > 1; i--)
            {
            if (trans.position == this.snake_body[i-1].position)
            {
                //print("DED");
                SnakeReset();
            }
        }
    }
        private void HandleWalls(Transform trans)
        {

            this.forward = trans.position + this.directionForward;
            this.left = trans.position + this.directionLeft;
            this.right = trans.position + this.directionRight;

            if (drawEyes)
            {
                forwardCircle.position = this.forward;
                leftCircle.position = this.left;
                rightCircle.position = this.right;
            }
            else
            {
                forwardCircle.position = new Vector3(50, 50, 0);
                leftCircle.position = new Vector3(50, 50, 0);
                rightCircle.position = new Vector3(50, 50, 0);
            }
            foreach (var bodypart in this.snake_body)
            {
                if (bodypart.position == this.forward)
                {
                    if (_movingHorizontal)
                    {
                        this.leftThreshold = 0f;
                        this.rightThreshold = 0f;
                        this.upThreshold = (this.doNothing / 2f) + 1f;
                        this.downThreshold = this.doNothing + 1f;
                        //print("A");
                    

                    }
                    if (_movingVertical)
                    {
                        this.upThreshold = 0f;
                        this.downThreshold = 0f;
                        this.leftThreshold = (this.doNothing / 2f) + 1f;
                        this.rightThreshold = this.doNothing + 1f;
                        //print("B");
                    
                    }
                }
                if (bodypart.position == this.left)
                {
                    if (_movingHorizontal)
                    {
                        if (bodypart.position.y <= trans.position.y)
                        {
                            this.upThreshold = this.doNothing;
                            this.downThreshold = 0f;
                            //print("c");
                        }
                        else //  if(this.left.y > 0)
                        {
                            this.upThreshold = 0f;
                            this.downThreshold = this.doNothing;
                            //print("d");
                        }
                    }
                    if (_movingVertical)
                    {
                        if (bodypart.position.x <= trans.position.x)
                        {
                            this.leftThreshold = 0.0f;
                            this.rightThreshold = this.doNothing;
                            //print("e");
                        }
                        else // (this.left.x < 0)
                        {
                            this.leftThreshold = this.doNothing;
                            this.rightThreshold = 0.0f;
                            //print("f");
                        }
                    }
                }
                if (bodypart.position == this.right)
                {
                    if (_movingHorizontal)
                    {
                        if (bodypart.position.y < trans.position.y)
                        {
                            this.upThreshold = this.doNothing;
                            this.downThreshold = 0f;
                            //print("g");

                        }
                        else
                        {
                            this.upThreshold = 0f;
                            this.downThreshold = this.doNothing;
                            //print("h");
                        }

                    }
                    if (_movingVertical)
                    {
                        if (bodypart.position.x < trans.position.x)
                        {
                            this.rightThreshold = this.doNothing;
                            this.leftThreshold = 0f;
                            //print("i");
                        }
                        else
                        {
                            this.rightThreshold = 0f;
                            this.leftThreshold = this.doNothing;
                            //print("j");
                        }

                    }
                }
            }


            if (Mathf.Abs(this.forward.y) >= this.boundY || Mathf.Abs(this.forward.x) >= this.boundX)
            {

                if (_movingHorizontal)
                {
                    this.leftThreshold = 0f;
                    this.rightThreshold = 0f;
                    this.upThreshold = (this.doNothing / 2f) + 1f;
                    this.downThreshold = this.doNothing + 1f;
                }
                if (_movingVertical)
                {
                    this.upThreshold = 0f;
                    this.downThreshold = 0f;
                    this.leftThreshold = (this.doNothing / 2f) + 1f;
                    this.rightThreshold = this.doNothing + 1f;
                }


            }

            if (Mathf.Abs(this.right.y) >= this.boundY || Mathf.Abs(this.right.x) >= this.boundX)
            {
                if (_movingHorizontal)
                {
                    if (this.right.y < 0)
                    {
                        this.upThreshold = this.doNothing;
                        this.downThreshold = 0f;

                    }
                    else
                    {
                        this.upThreshold = 0f;
                        this.downThreshold = this.doNothing;
                    }

                }
                if (_movingVertical)
                {
                    if (this.right.x < 0)
                    {
                        this.rightThreshold = this.doNothing;
                        this.leftThreshold = 0f;
                    }
                    if (this.right.x > 0)
                    {
                        this.rightThreshold = 0f;
                        this.leftThreshold = this.doNothing;
                    }

                }
            }

            if (Mathf.Abs(this.left.y) >= this.boundY || Mathf.Abs(this.left.x) >= this.boundX)
            {
                if (_movingHorizontal)
                {
                    if (this.left.y < 0)
                    {
                        this.upThreshold = this.doNothing;
                        this.downThreshold = 0f;
                    }
                    if (this.left.y > 0)
                    {
                        this.upThreshold = 0f;
                        this.downThreshold = this.doNothing;
                    }
                }
                if (_movingVertical)
                {
                    if (this.left.x > 0)
                    {
                        this.leftThreshold = this.doNothing;
                        this.rightThreshold = 0f;
                    }
                    if (this.left.x < 0)
                    {
                        this.leftThreshold = 0f;
                        this.rightThreshold = this.doNothing;
                    }
                }
            }
        }

        private void handleCollision()
        {
            if (Mathf.Abs(trans.position.x) >= this.boundX)
            {
                if (!GhostMode)
                {
                    SnakeReset();
                    return;
                }
                trans.position = new Vector3(trans.position.x * -1.0f, trans.position.y, 0);

            }
            if (Mathf.Abs(trans.position.y) >= this.boundY)
            {
                if (!GhostMode)
                {
                    SnakeReset();
                    return;
                }
                trans.position = new Vector3(trans.position.x, trans.position.y * -1.0f, 0);
            }
        }

        private void getDirection()
        {
            if (Input.GetKeyDown("w") && _movingHorizontal)
            {
                this.directionForward = new Vector3(0, 1, 0);
            }
            if (Input.GetKeyDown("s") && _movingHorizontal)
            {
                this.directionForward = new Vector3(0, -1, 0);
            }
            if (Input.GetKeyDown("a") && _movingVertical)
            {
                this.directionForward = new Vector3(-1, 0, 0);
            }
            if (Input.GetKeyDown("d") && _movingVertical)
            {
                this.directionForward = new Vector3(1, 0, 0);
            }
        }

        private void grow()
        {
            Transform segment = Instantiate(this.bodyPrefab);
            segment.position = snake_body[snake_body.Count - 1].position;
            snake_body.Add(segment);
        }

        private bool AutoPilot()
        {
            if (Input.GetKeyDown("space") && autoPilot)
            {
                autoPilot = false;
                //print("switch");
            }
            else if (Input.GetKeyDown("space") && !autoPilot)
            {
                autoPilot = true;
            }
            return autoPilot;
        }

        private bool movingHorizontal()
        {
            if (this.directionForward.x != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool movingVertical()
        {
            if (this.directionForward.y != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    } 
