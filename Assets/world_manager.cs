using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class world_manager : MonoBehaviour
{
    public int width;
    public int height;
    public static Action<int,int> onInit;

    public int groundSize;

    public Transform ground;
    public Transform upWall;
    public Transform downWall;
    public Transform rightWall;
    public Transform leftWall;

    private Slider widthSlider;
    private Slider heightSlider;
    private Button quitButton;
    


    // Start is called before the first frame update
    void Start()
    {
        quitButton = GameObject.Find("Quit Button").GetComponent<Button>();
        widthSlider = GameObject.Find("Width Slider").GetComponent<Slider>();
        heightSlider = GameObject.Find("Height Slider").GetComponent<Slider>();
        quitButton.onClick.AddListener(QuitApp);
    }

    void QuitApp()
    {
        Application.Quit();
        print("QUITERED");
    }

    // Update is called once per frame
    void Update()
    {
        width = (int)widthSlider.value;
        height = (int)heightSlider.value;
        onInit?.Invoke(width, height);
        ground.localScale = new Vector3(groundSize, groundSize, 0);
        upWall.position = new Vector3(0, height, 0);
        upWall.localScale = new Vector3(width * 2 + 1f, 1, 1);
        downWall.position = new Vector3(0, -height, 0);
        downWall.localScale = new Vector3((width * 2) + 1f, 1, 1);
        rightWall.position = new Vector3(width, 0, 0);
        rightWall.localScale = new Vector3(height * 2 + 1f, 1, 1);
        leftWall.position = new Vector3(-width, 0, 0);
        leftWall.localScale = new Vector3(height * 2 + 1f, 1, 1);

    }
}
